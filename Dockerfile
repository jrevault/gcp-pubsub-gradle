FROM google/cloud-sdk:latest

RUN apt-get install -y wget unzip zip

# JDK
RUN wget -O- https://apt.corretto.aws/corretto.key | apt-key add - && \
    echo 'deb https://apt.corretto.aws stable main' > /etc/apt/sources.list.d/corretto.list
RUN apt-get update -y && \
    apt-get install -y java-17-amazon-corretto-jdk

# GCloud run pub sub emulator
EXPOSE 8085

VOLUME /data

ENTRYPOINT ["gcloud", "beta", "emulators", "pubsub"]
CMD ["start", "--host-port=0.0.0.0:8085", "--data-dir=/data"]
